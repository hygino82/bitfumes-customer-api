package com.bitfumes.course.service;

import com.bitfumes.course.dto.CustomerDTO;
import com.bitfumes.course.entity.Customer;
import com.bitfumes.course.repository.CustomerRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
    private final ModelMapper mapper;
    private final CustomerRepository repository;

    public CustomerService(ModelMapper mapper, CustomerRepository repository) {
        this.mapper = mapper;
        this.repository = repository;
    }

    public Page<CustomerDTO> getAll(Pageable pageable) {
        Page<Customer> page = repository.getAll(pageable);

        return page.map(x -> mapper.map(x, CustomerDTO.class));
    }
}
