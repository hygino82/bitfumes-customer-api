package com.bitfumes.course.repository;

import com.bitfumes.course.entity.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    @Query("SELECT obj FROM Customer obj")
    Page<Customer> getAll(Pageable pageable);
}
