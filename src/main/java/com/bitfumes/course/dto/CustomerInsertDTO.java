package com.bitfumes.course.dto;

public class CustomerInsertDTO {

    private String name;
    private String email;

    public CustomerInsertDTO() {
    }

    public CustomerInsertDTO(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
