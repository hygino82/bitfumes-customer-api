package com.bitfumes.course.controller;

import com.bitfumes.course.dto.CustomerDTO;
import com.bitfumes.course.entity.Customer;
import com.bitfumes.course.service.CustomerService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/customer")
public class CostumerController {

    private final CustomerService service;

    public CostumerController(CustomerService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<Page<CustomerDTO>> getAll(Pageable pageable) {
        Page<CustomerDTO> pageDto = service.getAll(pageable);

        return ResponseEntity.ok(pageDto);
    }
}
